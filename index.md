---
title: Legacy Views
template: main-full.html
---

We've talked about Jetpack Compose for the entire class, but it's likely that you'll probably see code that uses the old "views" system.

In this module, we'll explore view-based user interfaces. The videos contained here are from an older (Summer 2020) face-to-face term, before I converted the content to use Jetpack Compose. (I started to write textual content but realized it would be more valuable to make the older videos available. Note that this is a lot of content, but you can select topics you're interested in from the playlist)

!!! Note

    The "Room" videos in this batch are similar in content to the earlier Room videos,
    but use `LiveData` for asynchronous data fetching rather than Kotlin `Flow`.


Video Playlist: [https://www.youtube.com/playlist?list=PLW-6wqFEcgTpc2rBCGhyFNksqaG0OhlvJ](https://www.youtube.com/playlist?list=PLW-6wqFEcgTpc2rBCGhyFNksqaG0OhlvJ)

Sample Code: [https://github.com/javadude/605.686-samples](https://github.com/javadude/605.686-samples)
